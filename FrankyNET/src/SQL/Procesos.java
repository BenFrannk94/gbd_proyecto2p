/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package SQL;

import getset.variables;
import java.sql.Connection;
import javax.swing.JOptionPane;
import java.sql.ResultSet;

/**
 *
 * @author benji
 */
public class Procesos extends Conection {
    java.sql.Statement st;    
    ResultSet rs;
    variables var= new variables();
    
    //CRUD Planes de Servicio
    public void insertar( String descripcion, String velocidad, String precio) {
        try {
            Connection conexion =  Conectar();
            st = conexion.createStatement();
            String sql = "Insert into plan_serv(plan_descripcion, plan_velocidad, plan_precio)" 
                    + "values('" + descripcion + "','" + velocidad + "','" + precio + "');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Registro exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Registro fallido" + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void consultar(String id){
        try {
            Connection conexion =Conectar();
            st = conexion.createStatement();
            String sql="select * from plan_serv where plan_id ='"+id+"'";
            rs=st.executeQuery(sql);
            if (rs.next()) 
            {
                var.setPlan_id(rs.getString("plan_id"));
                var.setPlan_descripcion(rs.getString("plan_descripcion"));
                var.setPlan_velocidad(rs.getString("plan_velocidad"));
                var.setPlan_precio(rs.getString("plan_precio"));
            }else{
                var.setPlan_id(rs.getString(""));
                var.setPlan_descripcion(rs.getString(""));
                var.setPlan_velocidad(rs.getString(""));
                var.setPlan_precio(rs.getString(""));
                JOptionPane.showMessageDialog(null, "No se encontro registro","Sin registro",JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de busqueda"+e,"Error busqueda",JOptionPane.ERROR_MESSAGE);

                var.setPlan_id("");
                var.setPlan_descripcion("");
                var.setPlan_velocidad("");
                var.setPlan_precio("");
        } 
    }
    
    public void actualizar(String id, String descripcion, String velocidad, String precio){
        try {
             Connection conexion =Conectar();
             st=conexion.createStatement();
             String sql="update plan_serv set plan_descripcion='"+descripcion+"',plan_velocidad='"+velocidad+"',"
                     + "plan_precio='"+precio+"' where plan_id='"+id+"'";
             st.executeUpdate(sql);
             JOptionPane.showMessageDialog(null, "Actualización Exitosa", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
             st.close();
            conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Actualización Fallida" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void eliminar(String id){
        try {
            Connection conexion =Conectar();
            st=conexion.createStatement();
            String sql="delete from plan_serv where plan_id='"+id+"' ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Eliminación exitosa", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Eliminación fallida" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    
    // CRUD Clientes 
    public void insertarcliente( String cedula, String nombre, String apellido, String nacimiento, String email, String telefono1,String telefono2, String direccion) {
        try {
            Connection conexion =  Conectar();
            st = conexion.createStatement();
            String sql = "Insert into cliente(cliente_cedula, cliente_nombre, cliente_apellido, cliente_nacimiento, cliente_email, cliente_telefono_1, cliente_telefono_2, cliente_direccion)" 
                    + "values('" + cedula + "','" + nombre + "','" + apellido + "','" + nacimiento + "','" + email + "','" + telefono1 + "','" + telefono2 + "','" + direccion + "');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Registro exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Registro fallido" + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void consultarcliente(String id){
        try {
            Connection conexion =Conectar();
            st = conexion.createStatement();
            String sql="select * from cliente where cliente_id ='"+id+"'";
            rs=st.executeQuery(sql);
            if (rs.next()) 
            {
                var.setCliente_id(rs.getString("cliente_id"));
                var.setCliente_cedula(rs.getString("cliente_cedula"));
                var.setCliente_nombre(rs.getString("cliente_nombre"));
                var.setCliente_apellido(rs.getString("cliente_apellido"));
                var.setCliente_nacimiento(rs.getString("cliente_nacimiento"));
                var.setCliente_email(rs.getString("cliente_email"));
                var.setCliente_telefono_1(rs.getString("cliente_telefono_1"));
                var.setCliente_telefono_2(rs.getString("cliente_telefono_2"));
                var.setCliente_direccion(rs.getString("cliente_direccion"));
            }else{
                var.setCliente_id(rs.getString(""));
                var.setCliente_cedula(rs.getString(""));
                var.setCliente_nombre(rs.getString(""));
                var.setCliente_apellido(rs.getString(""));
                var.setCliente_nacimiento(rs.getString(""));
                var.setCliente_email(rs.getString(""));
                var.setCliente_telefono_1(rs.getString(""));
                var.setCliente_telefono_2(rs.getString(""));
                var.setCliente_direccion(rs.getString(""));
                JOptionPane.showMessageDialog(null, "No se encontro registro","Sin registro",JOptionPane.INFORMATION_MESSAGE);}
            st.close();
            conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de busqueda"+e,"Error de busqueda",JOptionPane.ERROR_MESSAGE);
                var.setCliente_id("");
                var.setCliente_cedula("");
                var.setCliente_nombre("");
                var.setCliente_apellido("");
                var.setCliente_nacimiento("");
                var.setCliente_email("");
                var.setCliente_telefono_1("");
                var.setCliente_telefono_2("");
                var.setCliente_direccion("");
        } 
    }
    public void actualizarcliente(String id, String cedula, String nombre, String apellido, String nacimiento, String email, String telefono1,String telefono2, String direccion){
        try {
             Connection conexion =Conectar();
             st=conexion.createStatement();
             String sql="update cliente set cliente_cedula='"+cedula+"',cliente_nombre='"+nombre+"',cliente_apellido='"+apellido+"',cliente_nacimiento='"+nacimiento+"',"
                     + "cliente_email='"+email+"',cliente_telefono_1='"+telefono1+"',cliente_telefono_2='"+telefono2+"',"
                     + "cliente_direccion='"+direccion+"' where cliente_id='"+id+"'";
             st.executeUpdate(sql);
             JOptionPane.showMessageDialog(null, "Actualización exitosa", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
             st.close();
            conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Actualización fallida" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void eliminarcliente(String id){
        try {
            Connection conexion =Conectar();
            st=conexion.createStatement();
            String sql="delete from cliente where cliente_id='"+id+"' ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Eliminación exitosa", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Eliminación fallida" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
