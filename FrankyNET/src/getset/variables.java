/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package getset;

/**
 *
 * @author benji
 */
public class variables {
    //Variables para CRUD1 plan Servicios
    private static String plan_id;
    private static String plan_descripcion;
    private static String plan_velocidad;
    private static String plan_precio;
    

    public static String getPlan_id() {
        return plan_id;
    }

    public static void setPlan_id(String plan_id) {
        variables.plan_id = plan_id;
    }

    public static String getPlan_descripcion() {
        return plan_descripcion;
    }

    public static void setPlan_descripcion(String plan_descripcion) {
        variables.plan_descripcion = plan_descripcion;
    }

    public static String getPlan_velocidad() {
        return plan_velocidad;
    }

    public static void setPlan_velocidad(String plan_velocidad) {
        variables.plan_velocidad = plan_velocidad;
    }

    public static String getPlan_precio() {
        return plan_precio;
    }

    public static void setPlan_precio(String plan_precio) {
        variables.plan_precio = plan_precio;
    }
    
    
    //Variables para CRUD2 Clientes
    private static String cliente_id;
    private static String cliente_cedula;
    private static String cliente_nombre;
    private static String cliente_apellido;
    private static String cliente_nacimiento;
    private static String cliente_email;
    private static String cliente_telefono_1;
    private static String cliente_telefono_2;
    private static String cliente_direccion;

    public static String getCliente_id() {
        return cliente_id;
    }

    public static void setCliente_id(String cliente_id) {
        variables.cliente_id = cliente_id;
    }

    public static String getCliente_cedula() {
        return cliente_cedula;
    }

    public static void setCliente_cedula(String cliente_cedula) {
        variables.cliente_cedula = cliente_cedula;
    }

    public static String getCliente_nombre() {
        return cliente_nombre;
    }

    public static void setCliente_nombre(String cliente_nombre) {
        variables.cliente_nombre = cliente_nombre;
    }

    public static String getCliente_apellido() {
        return cliente_apellido;
    }

    public static void setCliente_apellido(String cliente_apellido) {
        variables.cliente_apellido = cliente_apellido;
    }

    public static String getCliente_nacimiento() {
        return cliente_nacimiento;
    }

    public static void setCliente_nacimiento(String cliente_nacimiento) {
        variables.cliente_nacimiento = cliente_nacimiento;
    }

    public static String getCliente_email() {
        return cliente_email;
    }

    public static void setCliente_email(String cliente_email) {
        variables.cliente_email = cliente_email;
    }

    public static String getCliente_telefono_1() {
        return cliente_telefono_1;
    }

    public static void setCliente_telefono_1(String cliente_telefono_1) {
        variables.cliente_telefono_1 = cliente_telefono_1;
    }

    public static String getCliente_telefono_2() {
        return cliente_telefono_2;
    }

    public static void setCliente_telefono_2(String cliente_telefono_2) {
        variables.cliente_telefono_2 = cliente_telefono_2;
    }

    public static String getCliente_direccion() {
        return cliente_direccion;
    }

    public static void setCliente_direccion(String cliente_direccion) {
        variables.cliente_direccion = cliente_direccion;
    }
    
}
