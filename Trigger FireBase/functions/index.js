const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
const db = admin.firestore();

exports.onUserCreate = functions.firestore.document('factura/{facturaId}').onCreate(async (snap, context) => {
    const values = snap.data();
    // Contar matriculas por id de nivel funciona
    const query = db.collection("factura");
    const snapshot = await query.where("cuenta_id","==",values.cuenta_id, 'tipo_pago_id', '==', 2).get();
    const count = snapshot.size;
    
    if(count < 3){
        console.log(`Interes no aplicado`)
    }
    else {
        const factur = await db
        .collection('factura')
        .where('factura_id', '==', values.factura_id)
            const item = await factur.get()
            let pago = 0
            item.forEach((querySnapshot) => (pago = querySnapshot.data().factura_total * 0.1 + querySnapshot.data().factura_total))
            const snapshot = await db
                .collection('factura')
                .where('factura_id', '==', values.factura_id)
                .get()
            let updatePromises = []
            snapshot.forEach((doc) => {
                updatePromises.push(
                    db
                        .collection('factura')
                        .doc(doc.id)
                        .update({ factura_total: pago })
                )
            })
            await Promise.all(updatePromises)
                console.log(`Interes aplicado`)
        }
})
